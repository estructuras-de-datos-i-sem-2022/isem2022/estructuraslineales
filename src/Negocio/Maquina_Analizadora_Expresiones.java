/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import util.colecciones_seed.Pila;

/**
 *
 * @author docenteauditorio
 */
public class Maquina_Analizadora_Expresiones {

    public Maquina_Analizadora_Expresiones() {
    }

    /**
     *
     * permite analizar si una expresión contiene paréntesis balenceados
     *
     * @param cadena una expresión para analizar
     * @return true si la cadena contiene una expresión con paréntesis
     * balanceados o false en caso contrario
     */
    public boolean tieneParatensisBalanceados(String cadena) {
        Pila<Character> p = new Pila();
        char v[] = cadena.toCharArray();
        for (char dato : v) {
            if (dato == '(') {
                p.push(dato);
            } else {
                if (dato == ')') {
                    if (!p.esVacia()) {
                        p.pop();
                    } else {
                        return false;
                    }
                }
            }
        }
        return p.esVacia();
    }

    /**
     * Método evalúa una expresión del tipo L=<a*b*>
     * donde se cumple que: 2a=b Ejemplo1: aab --> true Ejemplo1: aabb --> false
     * Ejemplo1: abaaba --> true Ejemplo1: abaabacde --> false--> NO CADENA L
     *
     * @param cadena
     * @return true si la cadena contiene el doble de a(s) que de b(s)
     */
    public boolean tieneDobleCantidad_A(String cadena) {
        
        /*
            NO SE PERMITE USAR CONTADORES, SOLO PILAS Y/O COLAS
        */
        return false;
    }

    /**
     * Orden: 1. []
     *         2-{}
     *          3. ()
     * 
     * Ejemplo1: [.....()]--> true
     * Ejemplo2: [.....({})]--> false
     * 
     * ayuda: LIBRO DE CESAR BECERRA DE ESTRUCTURAS DE DATOS EN C++ O JAVA 
     * 
     * @param cadena
     * @return 
     */
     public boolean tieneSignosAgrupacionBalanceados(String cadena) {
        
        /*
            NO SE PERMITE USAR CONTADORES, SOLO PILAS Y/O COLAS
        */
        return false;
    }

    
}
