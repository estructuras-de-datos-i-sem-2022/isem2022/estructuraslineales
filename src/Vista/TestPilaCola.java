/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import util.colecciones_seed.Cola;
import util.colecciones_seed.Pila;

/**
 *
 * @author docenteauditorio
 */
public class TestPilaCola {

    public static void main(String[] args) {
        Object estr[] = crearPila_Cola(JOptionPane.showInputDialog("Digite cadena"));
        imprimir(estr);
    }

    private static void imprimir(Object[] estructuras) {
        String msg1, msg2;
        msg1 = msg2 = "";

        Pila<Character> p = (Pila) (estructuras[0]);
        Cola<Character> c = (Cola) (estructuras[1]);

        //Esto se podría hacer en una sóla sentencia iterativa
        while (!p.esVacia()) {
            msg1 += p.pop() + "\t";
        }
        
         while (!c.esVacia()) {
            msg2 += c.deColar() + "\t";
        }
         System.out.println("MI pila:"+msg1);
         System.out.println("MI cola:"+msg2);

    }

    private static Object[] crearPila_Cola(String cadena) {
        Object[] estructuras = {new Pila(), new Cola()};

        char v[] = cadena.toCharArray();
        for (char dato : v) {
            ((Pila) (estructuras[0])).push(dato);
            ((Cola) (estructuras[1])).enColar(dato);
        }
        return estructuras;
    }
}
