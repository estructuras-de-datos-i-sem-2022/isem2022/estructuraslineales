/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.colecciones_seed.ListaS;

/**
 *
 * @author Auditorio
 */
public class TestInicidencia {

    public static void main(String[] args) {
        ListaS<Integer> l = new ListaS();
        l.insertarInicio(3);
        l.insertarInicio(5);
        l.insertarInicio(7);
        l.insertarInicio(9);
        l.insertarInicio(3);
        l.insertarInicio(5);
        l.insertarInicio(3);
        l.insertarInicio(5);
        // L=<9,7,5,3>

        ListaS<Integer> l2 = new ListaS();
        l2.insertarInicio(3);
//        l2.insertarInicio(5);
//        l2.insertarInicio(7);
//        l2.insertarInicio(9);
        System.out.println("L1:" + l.toString());
        System.out.println("L2:" + l2.toString());
        try {
            System.out.println("L2 en L1 está:" + l.getIncidencias(l2));
            System.out.println("L1 en L2 está:" + l2.getIncidencias(l));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
