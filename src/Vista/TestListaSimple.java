/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Estudiante;
import util.colecciones_seed.ListaS;

/**
 *
 * @author Auditorio
 */
public class TestListaSimple {

    public static void main(String[] args) {

        ListaS<Integer> l = new ListaS();
        l.insertarInicio(3);
        l.insertarInicio(5);
        l.insertarInicio(7);
        l.insertarInicio(9);
        // L=<9,7,5,3>

        ListaS<Integer> l2 = new ListaS();
        l2.insertarInicio(3);
        l2.insertarInicio(5);
        l2.insertarInicio(7);
        l2.insertarInicio(9);
        
        
        ListaS<Integer> l4 = new ListaS();
        l4.insertarFin(3);
        l4.insertarFin(5);
        l4.insertarFin(7);
        l4.insertarFin(9);
        
        System.out.println("Lista insertarFin -l4 \n:"+l4.toString());

        ListaS<Integer> l3 = new ListaS();
        l3.insertarInicio(3);
        l3.insertarInicio(5);
        l3.insertarInicio(7);

        ListaS<String> nombres1 = new ListaS();
        ListaS<String> nombres2 = new ListaS();

        nombres1.insertarInicio("Juan");
        nombres1.insertarInicio("Zaray");

        nombres2.insertarInicio("juan");
        nombres2.insertarInicio("zaray");

        //Listas de Objetos:  //(1, juan) 
        ListaS<Estudiante> estudiantes = new ListaS();
        estudiantes.insertarInicio(new Estudiante(1, "Juan"));
        estudiantes.insertarInicio(new Estudiante(2, "Zaray"));

        ListaS<Estudiante> estudiantes2 = new ListaS();
        estudiantes2.insertarInicio(new Estudiante(1, "Juan"));
        estudiantes2.insertarInicio(new Estudiante(2, "Zaray"));

        System.out.println("Lista de estudiantes:" + estudiantes.toString());

        System.out.println("Mi lista es:" + l.toString());
        if (l.equals(l2)) {
            System.out.println("Son iguales listas :)");
        } else {
            System.out.println("No Son iguales listas :(");
        }

        if (l.equals(l3)) {
            System.out.println("L3 con L Son iguales listas :)");
        } else {
            System.out.println("L3 con L No Son iguales listas :(");
        }

        if (nombres1.equals(nombres2)) {
            System.out.println("nombres Son iguales listas :)");
        } else {
            System.out.println("nombres No Son iguales listas :(");
        }

        if (estudiantes.equals(estudiantes2)) {
            System.out.println("Estudiantes Son iguales listas :)");
        } else {
            System.out.println("Estudiantes No Son iguales listas :(");
        }

        compararListas(l3, l2, "Enteros");
        compararListas(nombres1, nombres2, "String");
        compararListas(estudiantes, estudiantes2, "Persona");
    }

    public static <T> void compararListas(ListaS<T> l1, ListaS<T> l2, String name) {

        
        String name2=((Object)l1).getClass().getName();
        if (l1.equals(l2)) {
            System.out.println(name + "\t Son iguales listas :)");
        } else {
            System.out.println(name + "\t No Son iguales listas :(");
        }
        System.out.println(name2);
        System.out.println(l1.getNombreClaseInfo());
        

    }

}
