/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Iterator;
import util.colecciones_seed.ArbolBinario;
import util.colecciones_seed.NodoBin;
import util.graficador.BTreePrinter;

/**
 *
 * @author docenteauditorio
 */
public class TestArbolBinario {

    public static void main(String[] args) {
        ArbolBinario<String> nombres = new ArbolBinario();

        NodoBin<String> r1 = new NodoBin("D");
        NodoBin<String> r2 = new NodoBin("Z");
        NodoBin<String> r3 = new NodoBin("S");
        NodoBin<String> r4 = new NodoBin("C");
        NodoBin<String> r5 = new NodoBin("F");

        //Creando árboles:
        nombres.setRaiz(r3);
        r3.setDer(r2);
        r2.setIzq(r1);
        r3.setIzq(r4);
        r4.setDer(r5);

        BTreePrinter.printNode(nombres.getRaiz());

        /**
         * S
         * / \
         * / \
         * C Z
         *  /
         * D
         */
        imprimir(nombres.preOrden(), "PreOrden");
        imprimir(nombres.InOrden(), "InOrden");
        imprimir(nombres.posOrden(), "PosOrden");
        imprimir(nombres.getInfoIzquierdos(), "Infos del izquierdo");
        System.out.println("Luca(T)" + nombres.getLuca());
        System.out.println("Luca(T)" + nombres.getCardinalidad());
        System.out.println("Obtener Hojas Izquierdas: " + nombres.getleftLeaf());
        System.out.println("Nivel de D: " + nombres.getNivelElemento("D"));
        imprimir(nombres.getElementosNivel(1), "Niveles");
        imprimir(nombres.getRecorrido_Niveles(), "Recorrido por niveles");
        imprimir(nombres.getRecorrido_Niveles(2), "Recorrido por niveles dado un n");

    }

    private static void imprimir(Iterator<String> it, String msg) {
        System.out.println(msg);
        while (it.hasNext()) {
            System.out.print(it.next() + "\t");
        }
        System.out.println("");
    }
}
