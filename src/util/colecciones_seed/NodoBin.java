/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.colecciones_seed;

/**
 *
 * @author docenteauditorio
 */
public class NodoBin<T> {
    
    private T info;
    private NodoBin izq, der;

    public NodoBin(T info) {
        this.info = info;
    }

    public NodoBin() {
    }

    public NodoBin(T info, NodoBin izq, NodoBin der) {
        this.info = info;
        this.izq = izq;
        this.der = der;
    }

    
    
    
    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public NodoBin getIzq() {
        return izq;
    }

    public void setIzq(NodoBin izq) {
        this.izq = izq;
    }

    public NodoBin getDer() {
        return der;
    }

    public void setDer(NodoBin der) {
        this.der = der;
    }
    
    
    
}
