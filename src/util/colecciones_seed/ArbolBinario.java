/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.colecciones_seed;

import java.util.Iterator;

/**
 *
 * @author docenteauditorio
 */
public class ArbolBinario<T> {

    private NodoBin<T> raiz;

    /**
     * Obtiene la raíz del árbol
     *
     * @return
     */
    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    /**
     * Método decorador del preOrden de un árbol binario
     *
     * @return iterador de una lista circular doble con el recorrido preOrden
     */
    public Iterator<T> preOrden() {
        ListaCD<T> l = new ListaCD();
        this.preOrden(this.raiz, l);
        return l.iterator();
    }

    private void preOrden(NodoBin<T> r, ListaCD<T> l) {
        //caso base
        if (r == null) {
            return;
        }
        l.insertarFin(r.getInfo());
        this.preOrden(r.getIzq(), l);
        this.preOrden(r.getDer(), l);
    }

    public Iterator<T> InOrden() {
        ListaCD<T> l = new ListaCD();
        this.inOrden(this.raiz, l);
        return l.iterator();
    }

    private void inOrden(NodoBin<T> r, ListaCD<T> l) {
        //caso base
        if (r == null) {
            return;
        }

        this.inOrden(r.getIzq(), l);
        l.insertarFin(r.getInfo());
        this.inOrden(r.getDer(), l);
    }

    public Iterator<T> posOrden() {
        ListaCD<T> l = new ListaCD();
        this.posOrden(this.raiz, l);
        return l.iterator();
    }

    private void posOrden(NodoBin<T> r, ListaCD<T> l) {
        //caso base
        if (r == null) {
            return;
        }

        this.posOrden(r.getIzq(), l);
        this.posOrden(r.getDer(), l);
        l.insertarFin(r.getInfo());
    }

    public Iterator<T> getInfoIzquierdos() {
        ListaCD<T> l = new ListaCD();
        this.getInfoIzquierdos(this.raiz, l, false);
        return l.iterator();
    }

    private void getInfoIzquierdos(NodoBin<T> r, ListaCD<T> l, boolean izq) {
        //caso base
        if (r == null) {
            return;
        }
        if (izq) {
            l.insertarFin(r.getInfo());
        }
        this.getInfoIzquierdos(r.getIzq(), l, true);
        this.getInfoIzquierdos(r.getDer(), l, false);

    }

    /**
     * Obtiene la codificación Lukasiewicz
     *
     * @return un String con la codificación Lukasiewicz
     */
    public String getLuca() {
        return (getLuca(raiz));
    }

    private String getLuca(NodoBin<T> r) {
        if (r == null) {
            return "b";
        }
        return "a" + getLuca(r.getIzq()) + getLuca(r.getDer());
    }

    public int getCardinalidad() {
        return (getCardinalidad(raiz));
    }

    private int getCardinalidad(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        return 1 + getCardinalidad(r.getIzq()) + getCardinalidad(r.getDer());
    }

    public int getleftLeaf() {
        return getleftLeaf(this.raiz);
    }

    private int getleftLeaf(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        short h = 0;
        if (isLeaf(r.getIzq())) {
            h++;
        }
        return h + getleftLeaf(r.getIzq()) + getleftLeaf(r.getDer());
    }

    private boolean isLeaf(NodoBin<T> r) {
        return r != null && r.getIzq() == null && r.getDer() == null;
    }

    public int getNivelElemento(T info) {
        return getNivelElemento(this.raiz, info, -1);
    }

    private int getNivelElemento(NodoBin<T> r, T info, int nivelAnt) {
        if (r == null) {
            return 0;
        }
        if (r.getInfo().equals(info)) {
            return nivelAnt + 1;
        }
        return getNivelElemento(r.getIzq(), info, nivelAnt + 1)
                + getNivelElemento(r.getDer(), info, nivelAnt + 1);
    }

    public Iterator<T> getElementosNivel(int nivel) {
        ListaCD<T> lista = new ListaCD();
        getElementosNivel(raiz, nivel, 0, lista);
        return lista.iterator();
    }

    public void getElementosNivel(NodoBin<T> r, int nivel, int nivelActual, ListaCD<T> l) {
        if (r == null) {
            return;
        }
        if (nivel == nivelActual) {
            l.insertarFin(r.getInfo());
            return;
        }
        getElementosNivel(r.getIzq(), nivel, nivelActual + 1, l);
        getElementosNivel(r.getDer(), nivel, nivelActual + 1, l);
    }

    public Iterator<T> getRecorrido_Niveles() {

        if (this.esVacio()) {
            throw new RuntimeException("No es posible recorrer un árbol vacío");
        }
        ListaCD<T> l = new ListaCD();
        Cola<NodoBin<T>> c = new Cola();
        c.enColar(raiz);
        while (!c.esVacia()) {
            NodoBin<T> r = c.deColar();
            l.insertarFin(r.getInfo());
            if (r.getIzq() != null) {
                c.enColar(r.getIzq());
            }
            if (r.getDer() != null) {
                c.enColar(r.getDer());
            }

        }
        return l.iterator();

    }

    public Iterator<T> getRecorrido_Niveles(int n) {
        //n está entre 0 y H(T)
        if (this.esVacio() || n < 0) {
            throw new RuntimeException("No es posible recorrer un árbol vacío");
        }
        ListaCD<T> l = new ListaCD();
        Object aux[] = new Object[2];
        Cola<Object[]> c = new Cola();
        aux[0] = this.raiz;
        aux[1] = 0;
        c.enColar(aux);

        while (!c.esVacia()) {
            Object x[] = c.deColar();
            NodoBin<T> r = (NodoBin) (x[0]);
            Integer niv = (Integer) (x[1]);
            if (niv == n) {
                l.insertarFin(r.getInfo());
            }
            if (r.getIzq() != null) {
                c.enColar(this.encapsular(r.getIzq(), niv + 1));
            }
            if (r.getDer() != null) {
                c.enColar(this.encapsular(r.getDer(), niv + 1));
            }

        }
        return l.iterator();

    }

    private Object[] encapsular(NodoBin<T> r, int n) {
        Object aux[] = new Object[2];
        aux[0] = r;
        aux[1] = n;
        return aux;
    }

    public boolean esVacio() {
        return this.raiz == null;
    }
    
    
    public Iterator<T> preOrden_Iterativo()
    {
        return null;
    }
    
     public Iterator<T> inOrden_Iterativo()
    {
        return null;
    }
     
     
     public Iterator<T> posOrden_Iterativo()
    {
        return null;
    }
    
}
