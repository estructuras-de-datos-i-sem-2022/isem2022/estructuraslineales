/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.colecciones_seed;

/**
 *
 * @author docenteauditorio
 */
public class Pila<T> {

    private ListaCD<T> p = new ListaCD();

    public Pila() {
    }

    /**
     * Colocar un elemento en el tope de la pila
     *
     * @param info un objeto a insertar
     */
    public void push(T info) {
        this.p.insertarInicio(info);
    }

    public T pop() {
        if (this.p.esVacia()) {
            throw new RuntimeException("Pila vacía");
        }
        return this.p.borrar(0);
    }

    public int getSize() {
        return this.p.getSize();
    }

    public boolean esVacia() {
        return this.p.esVacia();
    }
}
