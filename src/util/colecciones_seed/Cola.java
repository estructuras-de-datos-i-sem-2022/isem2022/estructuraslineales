/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.colecciones_seed;

/**
 *
 * @author docenteauditorio
 */
public class Cola<T> {

    private ListaCD<T> p = new ListaCD();

    public Cola() {
    }

    /**
     * Colocar un elemento en el tope de la pila
     *
     * @param info un objeto a insertar
     */
    public void enColar(T info) {
        this.p.insertarFin(info);
    }

    public T deColar() {
        if (this.p.esVacia()) {
            throw new RuntimeException("Pila vacía");
        }
        return this.p.borrar(0);
    }

    public int getSize() {
        return this.p.getSize();
    }

    public boolean esVacia() {
        return this.p.esVacia();
    }
}
